![logo](images/terraform.logo.png)

## Terraform providers install and authentication options: ##

### In `main.tf`

    provider "aws" {
    region = "us-east-1"
    access_key = "ALAVERGAfge45STFMSOS"
    secret_key = "xeIqxsOYTALQUETELADI5A61ICPeM1kGl7j555"
    }

#### This is not a proper way to configure credentials, refer to **"Handling credential in config files"** section for a safer way to do this.

### In file `providers.tf`

 As a best practice you can define as many providers as you need to make use of them in `main.tf`

    terraform {
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.39.0"
        }
    }

    required_version = ">= 1.2.0"
    }

## Creating New Resources

### Issue the following command in the same folder where `main.tf` file is:

`$ terraform init`

This will create the provider's configuration under a hidden folder named `.terraform/`

If you want to confirm what actions are going to be executed do:

`$ terraform plan`

When you confirm those apply with:

`$ terraform apply`

### Taking values from existing resorces to instantiate new ones through terraform:

You can extract values as ids, names and other data by using data sources configuration:


    // REFERENCING AN EXISTING VPC (Default vpc) TO CREATE A NEW SUBNET IN IT

    data "aws_vpc" "existing-vpc" {
    default = true
    }

    resource "aws_subnet" "default-subnet-1" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.96.0/20"
    }

issue again the command to apply as:

`$ terraform apply`

### Another way to know the actual state of any resource already created in your code is:

`terraform state show ${resource}.${name}`

IE:

`terraform state show aws_vpc.dev-test-vpc`

### Changing or Removing Resources

Lets add environment tags to our resources created as an example:

    resource "aws_vpc" "dev-test-vpc" {
        cidr_block = "10.0.0.0/16"
        tags = {
        "Name" = "dev-test-app",
        "Environment" = "DEV"
        }
    }

To remove a reources you can either erase it from main.tf as a best practice since you should have version control for you IAC repository or pass:

`$ terraform destroy -target aws_subnet.default-subnet-1`

To destroy the following resource:

    resource "aws_subnet" "default-subnet-1" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.96.0/20"
    }

However the -target option is not for routine use, and is provided only for exceptional situations such as recovering from errors or mistakes, or when Terraform specifically suggests to use it as part of an error message.

### Destroy by resource

`$ terraform destroy -target aws_subnet.default-subnet-1`

`$ terraform destroy -target aws_subnet.dev-priv-subnet-2`

`$ terraform destroy -target aws_subnet.dev-priv-subnet-1`

`$ terraform destroy -target aws_vpc.dev-test-vpc`


### Resources and Data documentation

See AWS resource documentation in [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) to know how to use and define services in `main.tf`

![providers-usage](images/providers-usage.png)
![data-source](images/data-source.png)


## Terraform Commands

### Checking differences between actual state vs desired state

`$ terraform plan`

### Apply without confirmation

`$ terraform apply -auto-approve`

### Destroy all resources

`$ terraform destroy`

## Terraform States

file `terraform.tfstate`

When you need to update, create or destroy components it compares tha actual state by making api calls to providers with this file.

It stores as JSON the current state of your infrastructure when you apply a configuration and it's updated with each change.

file `terraform.tfstate.backup`

This file contains the previous configuration aplied so you can know how it was before actual config is applied.

### Terraform state argument

`$ terraform state`

list all components in `terraform.tfstate` file

![terraform-state](images/tf-state.png)

Later, with terraform show, you can get an IP from EC2, VPC ip or any other info about a resource that you may need for automatization

![terraform-show](images/tf-show.png)

## Terraform Outputs

This works as a return function:

Let say that we need to get the vpc id or any other info for the new created vpc:

 1. We do a `$ terraform plan` for the new component to select the infor we need within the available attributes

        # aws_vpc.dev-test-vpc will be created
        + resource "aws_vpc" "dev-test-vpc" {
            + arn                                  = (known after apply)
            + cidr_block                           = "10.0.0.0/16"
            + default_network_acl_id               = (known after apply)
            + default_route_table_id               = (known after apply)
            + default_security_group_id            = (known after apply)
            + dhcp_options_id                      = (known after apply)
            + enable_classiclink                   = (known after apply)
            + enable_classiclink_dns_support       = (known after apply)
            + enable_dns_hostnames                 = (known after apply)
            + enable_dns_support                   = true
            + id                                   = (known after apply)
            + instance_tenancy                     = "default"
            + ipv6_association_id                  = (known after apply)
            + ipv6_cidr_block                      = (known after apply)
            + ipv6_cidr_block_network_border_group = (known after apply)
            + main_route_table_id                  = (known after apply)
            + owner_id                             = (known after apply)
            + tags                                 = {
                + "Environment" = "DEV"
                + "Name"        = "dev-test-app"
                }
            + tags_all                             = {
                + "Environment" = "DEV"
                + "Name"        = "dev-test-app"
                }
            }


2. Select the attribute IE VPC id:

        output "dev-test-vpc-id" {
        value = aws_vpc.dev-test-vpc.id
        }

3. Do `$ terraform apply` and you should get at the end your vpc id once it's created:

        Outputs:

        dev-test-vpc-id = "vpc-0604e8f05a7c00c78"

## Terraform Variables

You can define one template to multiple use cases or environments relaying on variables and we have 3 ways to define values for them:

1. Passing the value as a parameter when `$ terraform apply` and the system prompts for input:

        variable "dev-test-vpc" {
        description = "VPC cidr for DEV environment"  
        }

        resource "aws_vpc" "dev-test-vpc" {
            cidr_block = var.dev-test-vpc
            tags = {
            "Name" = "dev-test-app",
            "Environment" = "DEV"
            }
        }

![vars-01](images/vars-1.png)

2. Using command line arguments as:

`$ terraform apply -var "dev-test-vpc=10.0.0.0/16"`

![vars-2](images/vars-2.png)

3. Assign a variables file named `terraform.tfvars` with key value pair like:

        test-vpc = "10.128.0.0/16"
        priv-subnet-1 = "10.128.10.0/24"
        environment = "DEV"

then issue `$ terraform apply` as usual.

This one is the way as a best practice!

Now, you can rename your `terraform.tfvars` file to `terraform-dev.tfvars` to customize it to a specific environment, then you need to pass that file as an argument when you need to apply it:

`$ terraform apply -var-file terraform-dev.tfvars`

You can also assign default values to the variables so you do not need to input for values every time you want to use the main.tf:

        variable "test-vpc" {
        description = "VPC cidr"
        default = "10.0.0.0/16"
        }

        variable "priv-subnet-1" {
        description = "Subnet cidr"  
        default = "10.0.10.0/24"
        }

        variable "environment" {
        description = "Environment tags"
        default = "test-use"  
        }

### Type Constraints


        variable "test-vpc" {
        description = "VPC cidr"
        default = "10.0.0.0/16"
        }

        variable "priv-subnet-1" {
        description = "Subnet cidr"  
        default = "10.0.10.0/24"
        }

        variable "environment" {
        description = "Environment tags"
        default = "test-use"  
        }

An important note is that it is not recommended to checkout you .tfvars file in yor repository as it may contain sensible information.

## Handling credential in config files

1. Setting credentials as environment variables as terraform is going to pick them from envars:

        $ export AWS_SECRET_ACCESS_KEY=ALLAAUAE4fge45STFMSOS
        $ export AWS_ACCESS_KEY_ID=xeIq4QkkxsO+I3465b48dh5A61ICPeM1kGl7j555 

Then issue your terraform commands.

2. Configuring aws credentials locally under `~/.aws/credentials`

`$ aws configure`

        AWS Access Key ID [None]: ALLAAUAE4fge45STFMSOS
        AWS Secret Access Key [None]: xeIq4QkkxsO+I3465b48dh5A61ICPeM1kGl7j555
        Default region name [None]: us-east-1
        Default output format [None]: json

`$ cat ~/.aws/credentials` 

        [default]
        aws_access_key_id = ALLAAUAE4fge45STFMSOS
        aws_secret_access_key = xeIq4QkkxsO+I3465b48dh5A61ICPeM1kGl7j555


See [aws docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) for reference to setting this up.

Example of credentials in Jenkins from [taiidani](https://registry.terraform.io/providers/taiidani/jenkins/latest/docs):

# Configure the Jenkins Provider
provider "jenkins" {
  server_url = "https://jenkins.url" # Or use JENKINS_URL env var
  username   = "username"            # Or use JENKINS_USERNAME env var
  password   = "password"            # Or use JENKINS_PASSWORD env var
  ca_cert = ""                       # Or use JENKINS_CA_CERT env var
}

So you can reference your credentials created in Jenkins on the `main.tf` file.

## Setting global variables in terraform

In example we want to configure an envar for AZ:

`$ export TF_VAR_avail_zone="us-east-1c"` being `avail_zone` the name of our envar, configi should look like:

        resource "aws_subnet" "priv-subnet-1" {
        vpc_id = aws_vpc.test-vpc.id
        cidr_block = var.priv-subnet-1
        availability_zone = var.avail_zone
        }

